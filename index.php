<?php
  require_once "koneksi.php";
 ?>
 <html>
   <head>
     <title>
       CRUD SEDERHANA
     </title>
   </head>
   <body>
    <form method="post" action="tambah.php">
    <input type="text" name="nama" placeholder="Nama">
    <input type="text" name="keterangan" placeholder="Keterangan">
    <input type="submit" name="submit" value="Tambah Data">
  </form><br/>
     <table border='1'>
       <thead>
         <tr>
           <th>
             No
           </th>
           <th>
             Nama
           </th>
           <th>
             Keterangan
           </th>
           <th>
             Ubah
           </th>
           <th>
             Hapus
           </th>
         </tr>
       </thead>
       <tbody>
         <?php
         $q = $conn->execute_query("SELECT id_peserta,nama,keterangan FROM peserta");
         $no = 1; // nomor urut
         while ($dt = $q->fetch_assoc()) :
          ?>
      <tr>
       <td><?= $no++ ?></td>
       <td><?= $dt['nama'] ?></td>
       <td><?= $dt['keterangan'] ?></td>
       <td><a href="ubah.php?id=<?=$dt['id_peserta'];?>">Ubah</a></td>
       <td><a href="hapus.php?id=<?=$dt['id_peserta']?>" onclick="return confirm('Anda yakin akan menghapus data ini?')">Hapus</a></td>
     </tr>
     <?php
     endwhile;
    ?>
       </tbody>
     </table>
   </body>
</html>
