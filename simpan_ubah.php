<?php
require_once 'koneksi.php';
if (isset($_POST['submit'])) {
  $id_peserta=$_POST['id_peserta'];
  $nama = $_POST['nama'];
  $keterangan = $_POST['keterangan'];
  $q = $conn->execute_query("UPDATE peserta SET nama=? , keterangan=? where id_peserta=?",[$nama,$keterangan,$id_peserta]);
  if ($q) {
    echo "<script>alert('Data produk berhasil diubah'); window.location.href='index.php'</script>";
  } else {
    echo $q->error();
    echo "<script>alert('Data produk gagal diubah'); window.location.href='index.php'</script>";
  }
} else {
  header('Location: index.php');
}
?>
