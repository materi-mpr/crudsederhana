<?php
require_once 'koneksi.php';
if (isset($_POST['submit'])) {
  $nama = $_POST['nama'];
  $keterangan = $_POST['keterangan'];
  $q = $conn->execute_query("INSERT INTO peserta (nama,keterangan) VALUES (?, ?)",[$nama,$keterangan]);
  if ($q) {
    echo "<script>alert('Data produk berhasil ditambahkan'); window.location.href='index.php'</script>";
  } else {
    echo $q->error();
    echo "<script>alert('Data produk gagal ditambahkan'); window.location.href='index.php'</script>";
  }
} else {
  header('Location: index.php');
}
?>
